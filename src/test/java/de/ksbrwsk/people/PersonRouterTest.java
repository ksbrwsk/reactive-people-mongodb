package de.ksbrwsk.people;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Optional;

import static de.ksbrwsk.people.PersonHandler.API;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebFluxTest
@Import({PersonHandler.class, PersonRouter.class})
class PersonRouterTest {

    @Autowired
    WebTestClient webTestClient;

    @MockBean
    PersonRepository personRepository;

    @Test
    void handleNotFound() {
        this.webTestClient
                .get()
                .uri("/api/peple")
                .exchange()
                .expectStatus()
                .isNotFound();
    }

    @Test
    void handleFindAll() {
        when(this.personRepository.findAll())
                .thenReturn(Flux.just(
                        new Person("1", "Name1"),
                        new Person("2", "Name2")
                ));
        this.webTestClient
                .get()
                .uri(API)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$[0].name").isEqualTo("Name1")
                .jsonPath("$[1].name").isEqualTo("Name2");
    }

    @Test
    void handleFindById() {
        when(this.personRepository.findById("1"))
                .thenReturn(Mono.just(new Person("1", "Name")));
        this.webTestClient
                .get()
                .uri(API + "/1")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(Person.class)
                .isEqualTo(new Person("1", "Name"));
    }

    @Test
    void handleFindByIdNotFound() {
        when(this.personRepository.findById("1"))
                .thenReturn(Mono.empty());
        this.webTestClient
                .get()
                .uri(API + "/1")
                .exchange()
                .expectStatus()
                .isNotFound();
    }

    @Test
    void handleDeleteById() {
        when(this.personRepository.findById("1"))
                .thenReturn(Mono.just(new Person("1", "Namer")));
        when(this.personRepository.delete(any(Person.class)))
                .thenReturn(Mono.empty());
        this.webTestClient
                .delete()
                .uri(API + "/1")
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$").isEqualTo("successfully deleted!");
    }

    @Test
    void handleDeleteByIdNotFound() {
        when(this.personRepository.findById("1"))
                .thenReturn(Mono.empty());
        this.webTestClient
                .delete()
                .uri(API + "/1")
                .exchange()
                .expectStatus()
                .isNotFound();
    }

    @ParameterizedTest
    @ValueSource(strings = {"N", "Name", "0123456789"})
    void handleUpdate(String name) {
        when(this.personRepository.findById("1"))
                .thenReturn(Mono.just(new Person("1", "Old")));
        when(this.personRepository.save(new Person("1", name)))
                .thenReturn(Mono.just(new Person("1", name)));
        this.webTestClient
                .put()
                .uri(API + "/1")
                .bodyValue(new Person("1", name))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(Person.class)
                .isEqualTo(new Person("1", name));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"00123456789"})
    void handleUpdateInvalid(String name) {
        when(this.personRepository.findById("1"))
                .thenReturn(Mono.just(new Person("1", "Old")));
        this.webTestClient
                .put()
                .uri(API + "/1")
                .bodyValue(new Person("1", name))
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @Test
    void handleUpdateNotFound() {
        when(this.personRepository.findById("1"))
                .thenReturn(Mono.empty());
        this.webTestClient
                .put()
                .uri(API + "/1")
                .bodyValue(new Person("1", "Update"))
                .exchange()
                .expectStatus()
                .isNotFound();
    }

    @Test
    void handleUpdateBadRequest() {
        when(this.personRepository.findById("1"))
                .thenReturn(Mono.just(new Person("1", "Name")));
        this.webTestClient
                .put()
                .uri(API + "/1")
                .bodyValue(Optional.empty())
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @ParameterizedTest
    @ValueSource(strings = {"N", "Name", "0123456789"})
    void handleCreate(String name) {
        when(this.personRepository.save(new Person(name)))
                .thenReturn(Mono.just(new Person("1", name)));
        this.webTestClient
                .post()
                .uri(API)
                .bodyValue(new Person(name))
                .exchange()
                .expectStatus()
                .isCreated()
                .expectHeader()
                .location(API + "/1")
                .expectBody(Person.class)
                .isEqualTo(new Person("1", name));
    }

    @ParameterizedTest
    @ValueSource(strings = {"00123456789"})
    @NullAndEmptySource
    void handleCreateInvalid(String name) {
        when(this.personRepository.save(new Person(name)))
                .thenReturn(Mono.just(new Person("1", name)));
        this.webTestClient
                .post()
                .uri(API)
                .bodyValue(new Person(name))
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @Test
    void handleCreateBadRequest() {
        this.webTestClient
                .post()
                .uri(API)
                .bodyValue(Optional.empty())
                .exchange()
                .expectStatus()
                .isBadRequest();
    }
}