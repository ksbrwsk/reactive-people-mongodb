package de.ksbrwsk.people;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.List;

@DataMongoTest(excludeAutoConfiguration = EmbeddedMongoAutoConfiguration.class)
class PersonRepositoryTestMongoDb extends AbstractMongoDbIntegrationTest {

    @Autowired
    PersonRepository personRepository;

    @Test
    void persist() {
        Mono<Long> longMono = this.personRepository
                .deleteAll()
                .then(this.personRepository.save(new Person("Name1")))
                .then(this.personRepository.save(new Person("Name2")))
                .then(this.personRepository.count());
        StepVerifier
                .create(longMono)
                .expectNext(2L)
                .verifyComplete();
    }

    @Test
    void findById() {
        Mono<Person> personMono = this.personRepository
                .deleteAll()
                .then(this.personRepository.save(new Person("Name")))
                .flatMap(person -> this.personRepository.findById(person.getId()));
        StepVerifier
                .create(personMono)
                .expectNextMatches(person -> person.getId() != null &&
                        person.getName().equalsIgnoreCase("name"))
                .verifyComplete();
    }

    @Test
    void delete() {
        Mono<Long> longMono = this.personRepository
                .deleteAll()
                .then(this.personRepository.save(new Person("Name")))
                .flatMap(this.personRepository::delete)
                .then(this.personRepository.count());
        StepVerifier
                .create(longMono)
                .expectNext(0L)
                .verifyComplete();
    }

    @Test
    void testdata() {
        List<Person> people = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            people.add(new Person("Person@" + i));
        }
        Flux<Person> personFlux = this.personRepository
                .deleteAll()
                .thenMany(this.personRepository.saveAll(people));
        StepVerifier
                .create(personFlux)
                .expectNextCount(100L)
                .verifyComplete();
    }
}