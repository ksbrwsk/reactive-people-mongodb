package de.ksbrwsk.people;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"spring.autoconfigure.exclude=" +
        "org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration"})
public class FullIntegrationTest extends AbstractMongoDbIntegrationTest {

    static WebTestClient webTestClient;

    @LocalServerPort
    int port;

    @Autowired
    PersonRepository personRepository;

    private WebTestClient getWebTestClient() {
        if (webTestClient == null) {
            webTestClient = WebTestClient
                    .bindToServer()
                    .baseUrl(String.format("http://localhost:%d/api/people", this.port))
                    .build();
        }
        return webTestClient;
    }

    @BeforeEach
    void beforeEach() {
        Mono<Long> longMono = this.personRepository
                .deleteAll()
                .then(this.personRepository.save(new Person("Name1")))
                .then(this.personRepository.save(new Person("Name2")))
                .then(this.personRepository.count());
        StepVerifier
                .create(longMono)
                .expectNext(2L)
                .verifyComplete();
    }

    @Test
    void handleFindById() {
        Person expected = this.personRepository
                .findAll()
                .take(1L)
                .blockFirst();
        this.getWebTestClient()
                .get()
                .uri("/" + expected.getId())
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.id").isEqualTo(expected.getId())
                .jsonPath("$.name").isEqualTo(expected.getName());
    }

    @Test
    void handleFindByIdNotFound() {
        this.getWebTestClient()
                .get()
                .uri("/4711")
                .exchange()
                .expectStatus()
                .isNotFound();
    }

    @Test
    void handleDeleteById() {
        Person expected = this.personRepository
                .findAll()
                .take(1L)
                .blockFirst();
        this.getWebTestClient()
                .delete()
                .uri("/" + expected.getId())
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$").isEqualTo("successfully deleted!");
    }

    @Test
    void handleDeleteByIdNotFound() {
        this.getWebTestClient()
                .delete()
                .uri("/4711")
                .exchange()
                .expectStatus()
                .isNotFound();
    }

    @ParameterizedTest
    @ValueSource(strings = {"N", "Name", "0123456789"})
    void handleUpdate(String name) {
        Person expected = this.personRepository
                .findAll()
                .take(1L)
                .blockFirst();
        this.getWebTestClient()
                .put()
                .uri("/" + expected.getId())
                .bodyValue(new Person(expected.getId(), name))
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.name").isEqualTo(name)
                .jsonPath("$.id").exists();
    }

    @ParameterizedTest
    @ValueSource(strings = {"00123456789"})
    @NullAndEmptySource
    void handleUpdateInvalid(String name) {
        Person expected = this.personRepository
                .findAll()
                .take(1L)
                .blockFirst();
        this.getWebTestClient()
                .put()
                .uri("/" + expected.getId())
                .bodyValue(new Person(expected.getId(), name))
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @ParameterizedTest
    @ValueSource(strings = {"N", "Name", "0123456789"})
    void handleCreate(String name) {
        this.getWebTestClient()
                .post()
                .uri("")
                .bodyValue(new Person(name))
                .exchange()
                .expectStatus()
                .isCreated()
                .expectHeader()
                .exists("Location")
                .expectBody()
                .jsonPath("$.name").isEqualTo(name)
                .jsonPath("$.id").exists();
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"00123456789"})
    void handleCreateInvalid(String name) {
        this.getWebTestClient()
                .post()
                .uri("")
                .bodyValue(new Person(name))
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @Test
    void handleFindAll() {
        this.getWebTestClient()
                .get()
                .uri("")
                .exchange()
                .expectStatus()
                .isOk()
                .expectHeader()
                .contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$[0].name").isEqualTo("Name1")
                .jsonPath("$[1].name").isEqualTo("Name2");
    }
}
