package de.ksbrwsk.people;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.*;

@Configuration
public class PersonRouter {
    @Bean
    RouterFunction<ServerResponse> http(PersonHandler personHandler) {
        return nest(path(PersonHandler.API),
                route(GET(""), personHandler::handleFindAll)
                        .andRoute(GET("/{id}"), personHandler::handleFindById)
                        .andRoute(DELETE("/{id}"), personHandler::handleDeletById)
                        .andRoute(PUT("/{id}"), personHandler::handleUpdate)
                        .andRoute(POST(""), personHandler::handleCreate)
        );
    }
}
